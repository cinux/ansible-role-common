"""Role testing files using testinfra."""


def test_hosts_file(host):
    """Check installed packages ."""

    packages = ['vim', 'tmux']
    for pkg in packages:
        assert host.package(pkg).is_installed


def test_hosts_user_creation(host):
    """Check user creation"""
    # unfortunatly the check file check is broken for some reason
    name = "testuser"
    shell = "/bin/bash"
    # keyfile = "/home/testuser/.ssh/authorized_keys"
    key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXyr/zGJFAg0XsEB6j1X364RFzIjBZ0qkaQ18YOCvZPeFRtCGZMaLOD7tPZz3y9Le75cNnc8WzxsJYk9GSlDK9yrUKADdd/cc9jk70ReH8xAYFWPyNQngReb3Pyj/IyY2FrO9RKQlSDCRDJKzdmHG2DgcfkeP3KPKIjPAfURxjwJsvuxkgZWQw7eErbGdP621b0TgnrAkTijM1j0xDD6B3Lg5hm/ZGIgz1kaP+8jTf4noDB5WDl7P38IXWNcUEpHNAvveBCTyF1kVAnri/rwtfhb5QrsEakxVi9jeDlPU61frcF6iVtAck4y1aW8UwQSQb79ejzH3LNVvXGiWT/I6MweXr5nbETxIbuxmaotWBBnHCY8chOaDFyjRgArFF2OH5VVkQwDu85r6YiLhGJ87gGfaisWugYwrJSFZ8ik99HeIk6zmwlB1rlhjm1txmwE40MLdnHCeUVQ3F7ZUAaUsdfub/8JHSdkblIzrKNF0FFGpXc6XO3/s4vh8p7yWN8UE= testuser"
    assert host.user(name).exists
    assert host.user(name).shell == shell
    # assert host.file("/home/testuser/.ssh/authorized_keys").exists
    # assert host.file("/home/testuser/.ssh/authorized_keys").contains(key)
