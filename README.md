common
=========

This role will install and configure tools for the daily work who should be available on every server.

Requirements
------------

no requirements

Role Variables
--------------

| Name                  | Default value | comment |
| :---------------------| :------------ | :------ |
| cinux_common_packages | <ul><li>vim</li><li>tmux</li></ul>        |         |



Dependencies
------------

no dependencies

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: cinux.common }

License
-------

[MIT](LICENSE)
